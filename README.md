# Atalian

A new Laravel project.

## Getting Started

How to run the app:

-   build docker container: docker-compose build app
-   run docker container and images: docker-compose up -d
-   install laravel package: docker-compose exec app composer install
-   generate new laravel key: docker-compose exec app php artisan key:generate
-   migrate database: docker-compose exec app php artisan migrate
-   install passport for laravel auth: docker-compose exec app php artisan passport:install
-   seed database for superadmin: docker-compose exec app php artisan db:seed
-   link storage folder: docker-compose exec app php artisan storage:link
-   install npm package for vue: docker-compose exec app npm install
-   test app via browser: http://ipaddress
    -   login with credential (email: admin@gmail.com, password: password)
