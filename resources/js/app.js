/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue").default;

import VueRouter from "vue-router";
import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";

Vue.use(VueRouter);
Vue.use(VueSweetalert2);

import App from "./components/App";
import Home from "./components/Home";
import Login from "./components/Login";
import Profile from "./components/Profile";
import DataUser from "./components/Users";
import AddUser from "./components/UsersAdd";
import Laporan from "./components/Laporan";

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "home",
            component: Home,
            meta: { requiresAuth: true }
        },
        {
            path: "/login",
            name: "login",
            component: Login,
            meta: { guest: true }
        },
        {
            path: "/profile",
            name: "profile",
            component: Profile,
            props: true,
            meta: { requiresAuth: true }
        },
        {
            path: "/data-user/:userRole",
            name: "data-user",
            component: DataUser,
            props: true,
            meta: { requiresAuth: true }
        },
        {
            path: "/add-user/:userRole",
            name: "add-user",
            component: AddUser,
            props: true,
            meta: { requiresAuth: true }
        },
        {
            path: "/laporan",
            name: "laporan",
            component: Laporan,
            props: true,
            meta: { requiresAuth: true }
        }
    ]
});

router.beforeEach((to, from, next) => {
    const token = localStorage.getItem("jwt") == null;
    if (to.matched.some(record => record.meta.guest)) {
        if (!token) next({ name: "home" });
        else next();
    }
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (token) {
            next({
                path: "/login",
                query: { redirect: to.fullPath }
            });
        } else {
            next();
        }
    } else {
        next();
    }
});

const app = new Vue({
    el: "#app",
    components: { App },
    router
});
