<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LaporanController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [UserController::class, 'login']);

Route::group(['middleware' => 'auth:api'], function(){
  Route::get('users/{type}', [UserController::class, 'users']);
  Route::post('users-add', [UserController::class, 'register']);
  Route::get('users-delete/{id}', [UserController::class, 'deleteUser']);

  Route::resource('laporan', LaporanController::class);

  Route::get('laporan-set/{id}/{teknisi}', [LaporanController::class, 'setTeknisi']);
  Route::get('laporan-done/{id}', [LaporanController::class, 'setDone']);
  Route::get('laporan-rating/{id}/{rating}', [LaporanController::class, 'setRating']);

});
