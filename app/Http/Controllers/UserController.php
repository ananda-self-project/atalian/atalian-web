<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Http\Resources\AppResources as AppResources;

class UserController extends BaseController
{

    public function login(Request $request)
    {  
        // array_merge($request->only('email', 'password'), ['type' => 1]);
        $credentials =  $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user(); 

            $success = $user;
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
   
            return $this->sendResponse($success, 'User login successfully.');
        }else{
            $status = 401;
            $response = ['error' => 'The email or password is incorrect.'];
        }

        return response()->json($response, $status);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:25',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|same:password',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 401);
        } 

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        
        $user = User::create($input);
        
        return $this->sendResponse($user, 'Berhasil menambah data');

    } 

    public function show(User $user)
    {
        return response()->json($user,200);
    }

    public function users(Request $request, $type = 1)
    {

        $data = User::where('type', $type)->get();

        return $this->sendResponse(AppResources::collection($data), 'Data.');
    }

    public function deleteUser($id) {
        $user =  Auth::user();
        if ($user->type == 2 || $user->type == 3) {
          return $this->sendResponse(
            [], 
            'Anda tidak memiliki akses untuk fitur ini.'
            );
        }

        $data = User::find($id);

        if ($data != null) {
            $data->delete();
            return $this->sendResponse([], 'Data berhasil dihapus');
        }else{
            return $this->sendResponse([], 'Data tidak ditemukan');
        }
    }

}
