<?php
   
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Models\User;
use App\Models\Laporan;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Resources\AppResources as AppResources;

class LaporanController extends BaseController
{

    public function index()
    {

        $user =  Auth::user();

        $data = [];
        
        if ($user->type == 1) $data = Laporan::orderBy('created_at', 'desc')->get();
        if ($user->type == 2) $data = Laporan::where('client', $user->id)->orderBy('created_at', 'desc')->get();
        if ($user->type == 3) $data = Laporan::where('teknisi', $user->id)->orderBy('created_at', 'desc')->get();

        $i = 0;
        if (count($data) > 0) {
            foreach($data as $dt) {

                $data[$i]["teknisi_name"] = "-";

                $c = User::find($data[$i]->client);
                $data[$i]["client_name"] = $c->name;

                $t = User::find($data[$i]->teknisi);
                if ($t != null) $data[$i]["teknisi_name"] = $t->name;

                $i++;
            }
        }

        return $this->sendResponse(
          AppResources::collection($data), 
        'Data Laporan.'
        );
    }

    public function store(Request $request)
    {

        $user =  Auth::user();
        if ($user->type == 1 || $user->type == 3) {
          return $this->sendError(
            'Anda tidak memiliki akses untuk fitur ini.'
            );
        }

        $input = $request->all();
   
        $validator = Validator::make($input, [
            'judul' => 'required',
            'deskripsi' => 'required',
            'photo' => 'required|mimes:png,jpg,jpeg',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        if ($file = $request->file('photo')) {
            $path = $file->storeAs('public/laporan', $user->id.'-'.time().'.'.$file->getClientOriginalExtension());
            $name = '/storage/laporan/'.$user->id.'-'.time().'.'.$file->getClientOriginalExtension();

            $input['photo'] = $name;
            $input['client'] = $user->id;
            $input['status'] = 1;

            $data = Laporan::create($input);
        }
   
        
   
        return $this->sendResponse(new AppResources($data), 'Berhasil menambah laporan.');
    }

    public function show($id)
    {
        $data = Laporan::find($id);
  
        if (is_null($data)) {
            return $this->sendError('Laporan tidak ditemukan.');
        }
   
        return $this->sendResponse(new AppResources($data), 'Detail Laporan.');
    }

    public function setTeknisi($id, $teknisi)
    {
        $data = Laporan::find($id);

        if ($data != null) {
            $data->teknisi = $teknisi;
            $data->status = 2;

            $data->save();

            $data["teknisi_name"] = "-";

            $c = User::find($data->client);
            $data["client_name"] = $c->name;

            $t = User::find($data->teknisi);
            if ($t != null) $data["teknisi_name"] = $t->name;
    
            return $this->sendResponse(new AppResources($data), 'Teknisi berhasil dipilih.');
        }else{
            return $this->sendResponse([], 'Laporan tidak ditemukan');
        }
    }

    public function setDone($id)
    {
        $data = Laporan::find($id);

        if ($data != null) {
            $data->status = 3;

            $data->save();

            $data["teknisi_name"] = "-";

            $c = User::find($data->client);
            $data["client_name"] = $c->name;

            $t = User::find($data->teknisi);
            if ($t != null) $data["teknisi_name"] = $t->name;
    
            return $this->sendResponse(new AppResources($data), 'Laporan berhasil diselesaikan.');
        }else{
            return $this->sendResponse([], 'Laporan tidak ditemukan');
        }
    }

    public function setRating($id, $rating)
    {
        $data = Laporan::find($id);

        if ($data != null) {
            $data->rating = $rating;

            $data->save();

            $data["teknisi_name"] = "-";

            $c = User::find($data->client);
            $data["client_name"] = $c->name;

            $t = User::find($data->teknisi);
            if ($t != null) $data["teknisi_name"] = $t->name;

            $data["rating"] = (int)$rating;
    
            return $this->sendResponse(new AppResources($data), 'Laporan berhasil diselesaikan.');
        }else{
            return $this->sendResponse([], 'Laporan tidak ditemukan');
        }
    }

}